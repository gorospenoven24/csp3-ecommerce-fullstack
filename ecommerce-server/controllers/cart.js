const Order =require ("../models/Order")
const Product =require ("../models/Product")
const User =require ("../models/User")
const Cart =require ("../models/Cart")
const bcrypt = require("bcrypt");
const auth = require("../auth");

// order model adding data
/*module.exports.getUserCart = async (reqBody)=>{

			

			let productId = reqBody.productId;
			let userId = reqBody.userId;
			let productName = reqBody.productName;
			let price = reqBody.price;
			let quantity = reqBody.quantity;
			let subTotal = quantity*price;
			let total = subTotal;
			let newCart = new Cart ({
				userId:userId,
				total: total,
				products :{
					productId:productId,
					productName:productName,
		 			price:price,
		 			quantity: quantity,
		 			subTotal:subTotal

				}
				
			})
			console.log(newCart)
			return newCart.save().then((result,error)=>{
				if (error) {
					return false;
				}else{
					
					return	true
				}
			})
			.catch(err => {
            console.log(err); // Remember to handle error to avoid hanging the request
            return false;
        });
	}*/


// get all order
	module.exports.allOrder= () => {
	return Cart.find({}).then(result => {
		return result;
	})
}

/* CART: Add to Cart
-------------------------------------------------------------*/
module.exports.addToCart = async (user, data) => {

			let subtotal = 0;
			let productId = data.productId;
			let price = data.price;
			let quantity = 1;
			subtotal += (price * quantity);

	let isUserUpdated = await User.findById(user)
		.then(user => {

			user.cart.push({
				productId: productId,
				price: price,
				quantity: quantity,
				subtotal: subtotal
			});
			return user.save().then((user, error) =>{
				console.log(user);
				if(error) {
					return false;
				}
				else {
					return true;
				}
			})
		})

	let isCartUpdated = await Cart.find({customerId: user})
		.then(cart => {

			let total = cart[0].total;
			total += subtotal;

			cart[0].items.push({
				productId: productId,
				price: price,
				quantity: quantity,
				subtotal: subtotal 
			});
			cart[0].total = total;
			return cart[0].save().then((cart, error) => {
				console.log(cart);
				if(error) {
					return false;
				}
				else {
					return true;
				}			
			})		
		})

		if(isUserUpdated && isCartUpdated) {return true;} 
			else {return false;}
}



	


