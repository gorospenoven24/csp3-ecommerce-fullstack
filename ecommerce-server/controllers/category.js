const Category = require("../models/Category");
const userController = require("../controllers/user");
const bcrypt = require("bcrypt");
const auth = require("../auth");


module.exports.addCategory = async (user,reqBody, res)=>{
	if(user.isAdmin){
		let newCategory = new Category({
			categoryName: reqBody.categoryName
			
		})
		return newCategory.save().then((category,error)=>{
		if (error) {
			return false;
		}else{
			
			return	true;
		}

	})

	}
	else{
		return("you have no access")
	}
}