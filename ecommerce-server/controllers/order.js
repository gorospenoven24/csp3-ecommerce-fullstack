const Order =require ("../models/Order")
const Product =require ("../models/Product")
const User =require ("../models/User")
const bcrypt = require("bcrypt");
const auth = require("../auth");

// display all order by admin
module.exports.getOrder = async (user,reqBody, res)=>{

		return Order.find().then((result,error)=>{
		if (error) {
			return false;
		}else{
			
			// Returns the user information with the password as an empty string
			return result;
		}
	})
	}

	
//  displaying order by specific user
module.exports.getUserOrder = async (user)=>{
	if(user.isAdmin == false){
		return Order.findOne({userId: user.id}).then(result=>{
			if (result == null) {
				return "you haave no items"
			}else{
				return	 result;
			}
			
		})
	} else{
		return("Log in first")
	}
}


// order model adding data
module.exports.order = async (userData, reqBody)=>{
	if(userData.isAdmin === false){
		let productID = await Product.findOne({_id: reqBody.productId}).then(result=>{
			//  adds the courseId in the usrs enrollemnt array
			return result.id;
		})
		let userID = await User.findOne({_id: userData.id}).then(result=>{
			//  adds the courseId in the usrs enrollemnt array
			return result.id;
		})
		let FirstName = await User.findOne({_id: userData.id}).then(result=>{
			//  adds the courseId in the usrs enrollemnt array
			return result.firstName;
		})
		let price = await Product.findOne({_id: reqBody.productId}).then(result =>{
			return result.price;
		})
		let name = await Product.findOne({_id: reqBody.productId}).then(result =>{
			return result.name;
		})


		let newOrder = new Order({
	 		totalAmount :price * reqBody.quantity,
	 		orderedBy: [{
	 			userId:userID,
	 			firstName:FirstName,
	 			productId:productID,
	 			name:name,
	 			price:price


	 		}]
	 	})

		// insert data into user model
		User.findOne({_id: userData.id}).then( result =>{
			let orderSumary = {
				orderId:productID

			}
			result.orders.push(newOrder);
			return result.save((result, error)=>{
				if (error) {
					return false;
				}else{
					
					return	true;
				}
			})
		})	

		// save data into order model
			return newOrder.save().then((order,error)=>{
				if (error) {
					return false;
				}else{
					
					return	("Order created successfuly");
				}
		})
		
		

	}
	else{
		return("you have no access")
	}
}