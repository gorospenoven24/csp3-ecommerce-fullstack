const User = require("../models/User");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Check if the email already exists
module.exports.checkEmailExists = (reqBody) => {	
	return User.find({email: reqBody.email}).then(result => {

		if(result.length > 0){
			return true;
		}
		else{
			return false;
		}
	})
}

// User Registration
module.exports.registerUser = (reqBody) => {
	// Creates a variable "newUser" and instantiates a new "User" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName:reqBody.lastName,
		address:reqBody.address,
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10),
		isAdmin : reqBody.isAdmin
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

// User authentication
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
	
		if(result == null){
			//console.log(result);
			return undefined;
		}

		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result) }
			}
			else{
				return  false;

			}
		}
	})

}

// set uer as admin

module.exports.setAdmin = async (userData, reqParams) => {
	if(userData.isAdmin){
		return User.findByIdAndUpdate({_id: reqParams.userId}, {isAdmin: true}).then(result =>{
			return result.save().then((updatedResult, error) =>{
				if(error){
					return `Failed to process request.`;
				} else{
					return `Successfully updated.`;
				}
			})
		})
	} else{
		return `You have no access`;
	}
}


//Display users by admin only

module.exports.getProfile = async (user, res)=>{
		return User.findById(user.id).then((result,error)=>{
		if (error) {
			return false;
		}else{
			
			result.password = "";
			// Returns the user information with the password as an empty string
			return result;
		}
	})
}


// Get all user
module.exports.allUser = () => {
	return User.find({}).then(result => {
		return result;
	})
}


// /////////////////////////
/*module.exports.order = async (userData, data)=>{
	if(userData.isAdmin == false){
		let isOrderUpdated = await User.findById(data.userId).then(user=>{
			
			//  adds the productId in the orders array in the user
			user.orders.push({productId :data.productId});
			// save the updated user info in the data base
			return order.save().then((order,error)=>{
				if(error){
					return false;
				}else{
					return true;
				}
				
			})
		})

			// add the user id in the whoOrdered array of the product 
		let isProductUpdated = await Product.findById(data.productId).then(product=>{
			// adds the user in the products whoOrdered array at the user ID 
			product.whoOrdered.push({userId:data.userId});

			// save the updated product info in the data base
			return product.save().then((product,error)=>{
				if(error){
					return false;
				}else{
					return true
				}
			})
		})


		//  condition that will check if the user and course documents have been updated
		if(isOrderUpdated && isProductUpdated){
			return true;
		}
		else{
			return false;
		}
	}
	else{
		return("you have no access")
	}
}*/