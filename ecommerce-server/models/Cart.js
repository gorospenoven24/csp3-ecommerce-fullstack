const mongoose = require('mongoose')

const cartSchema = new mongoose.Schema({
	customerId: {
		type: String },
	items: [{
		productId: {type: String},
		price: Number,
		quantity: Number,
		subtotal: Number,
		addedOn: {
			type: Date,
			default: Date.now },
		isActive: {
			type: Boolean,
			default: true },
		isCheckedOut: {
			type: Boolean,
			default: false } 
	}],
	total: Number,
})

module.exports = mongoose.model('Cart', cartSchema);

















// const mongoose = require("mongoose");

// //  Creat euser with the following properties
// const cartSchema = new mongoose.Schema({
// 	userId : {
// 		type : String,
// 		// required:[true, "User id is required"]
// 	},
// 	products:[{
// 	productId : {
// 		type : String,
// 		required:[true, "User id is required"]
// 	},
// 	productName : {
// 		type : String,
// 		required:[true, "User id is required"]
// 	},
// 	price : {
// 		type : String,
// 		required:[true, "User id is required"]
// 	},
// 	subTotal : {
// 		type : Number,
// 		required:[true, "User id is required"]
// 	}
// 	}],
// 	total:{
// 			type : Number
// 	}
	
	
// 	});

// module.exports = mongoose.model("Cart", cartSchema);
