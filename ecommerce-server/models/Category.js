const mongoose = require("mongoose");

//  Creat euser with the following properties
const categorySchema = new mongoose.Schema({
	categoryName : {
		type : String,
		required : [true, "Category is required"]
	}
});
	

module.exports = mongoose.model("Category", categorySchema);
