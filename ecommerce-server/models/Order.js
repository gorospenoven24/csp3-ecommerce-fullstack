const mongoose = require("mongoose");

//  Creat euser with the following properties
const orderSchema = new mongoose.Schema({
	totalAmount : {
		type : Number,
		required : [true, "Email is required"]
	},
	purchasedOn : {
		type : Date,
		default: new Date()
	},
	orderedBy: [{
			userId : {
				type: String
			},
			firstName: {
				type: String
			},
			orderedDate: {
				type: Date,
				default: new Date()
			},
			productId : {
				type: String
			},
			name: {
				type: String
			},
			price:{
				type: String
			}

	}],
	
	});

module.exports = mongoose.model("Order", orderSchema);
