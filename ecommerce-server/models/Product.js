const mongoose = require("mongoose");

//  Creat euser with the following properties
const productSchema = new mongoose.Schema({
	name : {
		type : String,
		required : [true, "name is required"]
	},
	description : {
		type : String,
		required : [true, "description is required"]
	},
	image : {
		type : String,
		required : [true, "image is required"]
	},
	price : {
		type : Number,
		required : [true, "price is required"]
	},
	isActive : {
		type : Boolean,
		default : true
	},
	createdOn : {
		type: Date,
		default: new Date()
	},
	whoOrdered:[{
		orderId:{
			type: String
		}
	}]
});
	

module.exports = mongoose.model("Product", productSchema);
