const mongoose = require("mongoose");

//  Creat euser with the following properties
const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		required : [true, "Email is required"]
	},
	lastName : {
		type : String,
		required : [true, "Email is required"]
	},
	address : {
		type : String,
		required : [true, "Address is required"]
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	orders:[{
		orderId:{
			type: String
		}
		
		
	}],
	cart: [{
			productId: {type: String},
			price: {type: Number},
			quantity: {type: Number},
			subtotal: {type: Number},
			addedOn: {
				type: Date,
				default: Date.now }
		}]




});
	
module.exports = mongoose.model("User", userSchema);
