const express = require("express");
const router = express.Router();
const categoryController = require("../controllers/category");
const auth = require("../auth");


// Route for Registering Category 
router.post("/category", auth.verify, (req, res) => {
			const data = auth.decode(req.headers.authorization)
			categoryController.addCategory(data, req.body).then(resultFromController => res.send(resultFromController));

		});

module.exports = router;