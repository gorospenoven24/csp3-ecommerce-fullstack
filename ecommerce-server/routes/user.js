const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const orderController = require("../controllers/order");
const auth = require("../auth");

// Route for checking if the user's email already exists in the database
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for registering a user
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for authenticating a user
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})


/*// Route for updating  a user 
router.put("/:id/setAdmin", (req,res)=>{
	userController.updateUser(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})*/


router.put("/:userId/setAdmin", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.setAdmin(userData, req.params).then(resultFromController => res.send(resultFromController));
});




// auth verify method acts as a middleware to ensure the user is logged in before they can access specific app features
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	// provides the users ID for the getprofile controller method
	userController.getProfile(userData).then(resultFromController => res.send(resultFromController));
})

router.get("/alluser",(req,res)=>{
	userController.allUser().then(resultFromController => res.send(resultFromController	));
})


router.post("/checkOut",auth.verify,(req,res)=>{
	const userData = auth.decode(req.headers.authorization);
	orderController.order(userData, req.body).then(resultFromController => res.send(resultFromController));

})
// get orders by admin
router.get("/orders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	// provides the users ID for the getprofile controller method
	orderController.getOrder(userData).then(resultFromController => res.send(resultFromController));
})
// checkou orders by users
router.get("/myOrders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	// provides the users ID for the getprofile controller method
	orderController.getUserOrder(userData).then(resultFromController => res.send(resultFromController));
})


// Allows us to export the "router" object that will be accessed in "index.js"\
module.exports = router;