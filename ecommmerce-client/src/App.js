import { Container, } from 'react-bootstrap';
import { Fragment } from 'react';
import AppNavbar from './components/AppNavbar';
// import Banner from './components/Banner';
import {BrowserRouter as Router} from 'react-router-dom';
import {Route, Switch} from 'react-router-dom';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Admin from './pages/Admin';
import Logout from './pages/Logout';
import { UserProvider } from './UserContext';
import {useState, useEffect } from 'react';
import Error from './pages/Error';
import AddProduct from './pages/AddProduct';
import CartView from './pages/Cart';
import AdminUser from './pages/AdminUser';
import AdminOrder from './pages/AdminOrder';
import ProductDetails from './pages/productDetails'
import OrderHistory from './pages/OrderHistory'

// import ProductList from './pages/Home'
//import {Route, Switch} from 'react-router-dom';
import './App.css';



function App() {

  const [user, setUser] = useState({
  id: null,
  isAdmin:null
  })

  const unsetUser =() =>{
    localStorage.clear();
  }
    useEffect(() => {
      console.log(user);
      console.log(localStorage);
    },[user])

  return (
  
         <UserProvider value={{user, setUser, unsetUser}}>
          <Router>
          <AppNavbar />
            <Switch>
            
               <Route exact path="/" component ={Home}/>
                <Route exact path="/register" component ={Register}/>
                <Route exact path="/login" component ={Login}/>
              
                <Route exact path="/orderhistory" component ={OrderHistory}/>       
                  <Route exact path="/logout" component ={Logout}/>
                   <Route exact path="/products/:productId" component={ProductDetails} />
                   <Route exact path="/cart" component={CartView} />


                    {(user.isAdmin === true)?
                        <Fragment>
                           <Route exact path="/admin" component ={Admin}/>
                            <Route exact path="/addproduct" component ={AddProduct}/>
                             <Route exact path="/alluser" component ={AdminUser}/>
                             <Route exact path="/allorder" component ={AdminOrder}/>
                            </Fragment>
                           
                    :
                    <Route component={Error} />
                  }
                    
              {/*<Route exact path="/courses" component ={Courses}/>
              <Route exact path="/courses/:courseId" component ={CourseView}/>
              <Route exact path="/login" component ={Login}/>
              <Route exact path="/logout" component ={Logout}/>
              <Route exact path="/register" component ={Register}/>
              <Route component={Error} />*/}
            </Switch>
          </Router>
        </UserProvider>
    
  );
}

export default App;
