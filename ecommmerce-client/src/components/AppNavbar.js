import React from 'react';
import { Fragment, useContext } from 'react'
import Navbar from 'react-bootstrap/Navbar';
import { Container} from 'react-bootstrap';
import Nav from 'react-bootstrap/Nav';
import {Link, NavLink} from 'react-router-dom';
import UserContext from '../UserContext';



export default function AppNavbar(){
	const { user }=useContext(UserContext);
	return (	
			(user.isAdmin === true)?
				<div style={{backgroundColor:"#f02d1f", color:"white"}}>
					<Navbar collapseOnSelect expand="lg"  variant="dark">

		                <Navbar.Brand>ShopNow Admin Dashboard</Navbar.Brand>
		                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
		                <Navbar.Collapse id="responsive-navbar-nav">
		               		 <Nav>
		                        {/*<Nav.Link as={NavLink} to="/">Product List</Nav.Link>
		                        <Nav.Link >Category</Nav.Link>*/}
		                          {/*<Nav.Link as={NavLink} to="/admin">Admin Dashboard</Nav.Link>*/}
		                    </Nav>
		                   
		                </Navbar.Collapse>
		           	 </Navbar>
		            </div>
				:
				<div style={{backgroundColor:"#f02d1f", color:"white"}}>
							<Navbar collapseOnSelect expand="lg"  variant="dark">

				                <Navbar.Brand as={NavLink} to="/">ShopNow</Navbar.Brand>
				                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
				                <Navbar.Collapse id="responsive-navbar-nav">
				               		 <Nav>
				                        <Nav.Link as={NavLink} to="/">Product List</Nav.Link>
				                        <Nav.Link >Category</Nav.Link>
				                        <Nav.Link href="">About</Nav.Link>

				                          {/*<Nav.Link as={NavLink} to="/admin">"Temporary link for Admin"</Nav.Link>*/}
				                    </Nav>
				                    <Nav className="ml-auto">
				                        
				                         {(user.id !== null) ?
				                         <Fragment>	
				                            <Nav.Link as={NavLink} to="/cart" exact>Cart</Nav.Link>
				                           <Nav.Link as={NavLink} to="/orderhistory" exact>OrderHistory</Nav.Link>
						      		 <Nav.Link as={NavLink} to="/logout" exact>Logout</Nav.Link>

						      		 </Fragment>
						      	 	:
						      	 	 <Fragment>
				                        <Nav.Link as={NavLink} to="/login" exact>Login</Nav.Link>
							      		 <Nav.Link as={NavLink} to="/register" exact>Register</Nav.Link>
				                         </Fragment>
						      		} 
						      
				                    </Nav>
				                </Navbar.Collapse>
				           	 </Navbar>
				            </div>
				

			
				
		        

	
		)	
	    
}