/*import React from 'react';
import { Fragment } from 'react'
import { Container} from 'react-bootstrap';
import Badge from 'react-bootstrap/Badge'
import {Link, NavLink} from 'react-router-dom';
import { Row, Col, Card } from 'react-bootstrap';
import Navbar from 'react-bootstrap/Navbar';
// import { Container} from 'react-bootstrap';
import Nav from 'react-bootstrap/Nav';
import Form from 'react-bootstrap/Form'
import style from '../App.css'

export default function Banner(){
	return (
		<Container className="mt-5">
		  <Row>
		    <Col sm={12}>
		    <h1 style={{color:"red"}}>	Welcome To shopNow Enjoy Shopping</h1>
		    </Col>
		  </Row>
		</Container>


		)
}

*/
import {Link} from 'react-router-dom';
import { Button, Row, Col, } from 'react-bootstrap';

export default function banner({data}) {

		const {title, content, destination, label} = data;
	
		return (
			<Row>
				<Col className="p-5">
					<h1 style={{color:"red"}}>{title}</h1>
					<p>{content}</p>
					<Link variant="primary" to={destination}>{label}</Link>
					{/*<Button variant="primary" to={destination}>{label}</Button>*/}
				</Col>
			</Row>
		)

}