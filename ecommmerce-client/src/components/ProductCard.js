//import { useState } from 'react';
// Proptypes - use to validate props
import { useState, useEffect, useContext } from 'react';
import PropTypes from 'prop-types'
import { Row, Col, Card, CardGroup } from 'react-bootstrap';
import   {Link} from 'react-router-dom'
import { Container, Form} from 'react-bootstrap';
import ProductDetails from '../pages/productDetails';

export default function Product({productProp}){
	//console.log(props);
	const {_id, name, description, price, image} = productProp;
	const [ product, setProduct] = useState([]);

	const getDetails = () => {
		let id = this._id;
		let name = this.name;
		let description = this.description;
		let price = this.price;
		setProduct(id, name, description, price);

		ProductDetails(product);
	}



	return (
		<Container fluid>
	
			<CardGroup xs={12} md={6} lg={3}  className="pt-3 float"	>
				<Card className="cardHighlight" style={{ width: '18rem' }}>
				 
				  <Card.Img variant="top" src={image} />
				  <hr></hr>
				    <Card.Title className="ml-2" style={{color:"green"}}>{name}</Card.Title>
				    <Card.Subtitle className="mt-1 ml-2">Description: </Card.Subtitle>
				   <Card.Text  className="mt-1 ml-3">{description}</Card.Text>
				   {/*<Card.Subtitle>Price: </Card.Subtitle>
				   <Card.Text>{price}</Card.Text>*/}
				   			<Card.Subtitle as="h5" className="mt-1 ml-1"> 
						  			<Form.Label className="mr-3">
      									Price:
      								</Form.Label>
      								<Form.Label>
      									{price}
      								</Form.Label>
      						</Card.Subtitle>
				   <hr></hr>
				   <Link className="btn btn-success ml-4"  style={{width:220}} onPress={getDetails} to={`/products/${_id}`}>Details</Link>
				   {/*<Link className= "btn btn-primary" to ={`productdetails/${_id}`} >Details</Link>  */}
				    <hr></hr>
				</Card>
		</CardGroup>
	
		</Container>
		)
	}

	// checks the validity of the PropTypes
	Product.propTypes = {
		// "shape" mtehod is to check if the prop object conforms to a sepecific shape
		product: PropTypes.shape({
			name: PropTypes.string.isRequired,
			description: PropTypes.string.isRequired,
			price: PropTypes.string.isRequired
		})
	}