import React from 'react';
import { Fragment } from 'react'
import { Container} from 'react-bootstrap';
import Badge from 'react-bootstrap/Badge'
import {Link, NavLink} from 'react-router-dom';
import { Row, Col, Card } from 'react-bootstrap';
import Navbar from 'react-bootstrap/Navbar';
// import { Container} from 'react-bootstrap';
import Nav from 'react-bootstrap/Nav';
import Form from 'react-bootstrap/Form'
import style from '../App.css'

export default function SecondNavbar(){
	return (

		
		<div style={{backgroundColor:"#f55c51", color:"white"}} className="mt-1">
		  	<Navbar>
			    <Container>
			    <Nav>
			      <Nav.Link style={{color:"white"}} href="#home">Men Apparels</Nav.Link>
			      <Nav.Link style={{color:"white"}} href="#features">Women Apparels</Nav.Link>
			      <Nav.Link  style={{color:"white"}} href="#pricing">Kid Apparel</Nav.Link>
			      <Nav.Link  style={{color:"white"}} href="#pricing">Gadget</Nav.Link>
			       <Nav.Link style={{color:"white"}}  href="#pricing">Appliances</Nav.Link>
			    </Nav>
			    <nav className="ml-auto">
			     <Form >
			           <Form.Control type="text" placeholder="Search Product" />
			           </Form>
			    </nav>
			    </Container>
			  </Navbar>
			  </div>
		
	
		)
}