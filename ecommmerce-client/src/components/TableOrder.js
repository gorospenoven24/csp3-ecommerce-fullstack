//import { useState } from 'react';
// Proptypes - use to validate props
import PropTypes from 'prop-types'
import { Row, Col, Card, CardGroup, tr, td, th, thead,Table, tbody } from 'react-bootstrap';
import   {Link} from 'react-router-dom'
import { Container} from 'react-bootstrap';

export default function Cart({cartProp}){
	//console.log(props);
	const {_id, userId, productId, productName, quantity, total, subTotal} = cartProp;
	return (
		<Container fluid>
			 <Table striped bordered hover size="sm">
		         <tbody>
		        <tr>
		          <td className = "tdName">{userId}</td>
		          <td className = "tdDescription">{productId}</td>
		          <td className = "tdPrice">{productName}</td>
		           <td className = "tdIsactive">{quantity}</td>
		             <td className = "tdIsactive">{subTotal}</td>
		             <td className = "tdIsactive">{total}</td>
		        </tr>
		        </tbody>
		     
		      </Table>
			
		</Container>
		)
	}

	// checks the validity of the PropTypes
	Cart.propTypes = {
		// "shape" mtehod is to check if the prop object conforms to a sepecific shape
		cart: PropTypes.shape({
			userId: PropTypes.string.isRequired,
			productId: PropTypes.string.isRequired,
			productName: PropTypes.string.isRequired,
			quantity: PropTypes.string.isRequired,
			subTotal: PropTypes.string.isRequired,
			total: PropTypes.string.isRequired
		})
	}