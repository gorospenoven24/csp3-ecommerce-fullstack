//import { useState } from 'react';
// Proptypes - use to validate props
import PropTypes from 'prop-types'
import { Row, Col, Card, CardGroup, tr, td, th, thead,Table, tbody } from 'react-bootstrap';
import   {Link} from 'react-router-dom'
import { Container} from 'react-bootstrap';

export default function User({userProp}){
	//console.log(props);
	const {_id, firstName, lastName, address, email, isAdmin} = userProp;
	return (
		<Container fluid>
			 <Table striped bordered hover size="sm">
		     {/* <thead>
		        <tr>
		        
		          <th>Name</th>
		          <th>Description</th>
		          <th>Price</th>
		           <th>Status</th>
		        </tr>
		         </thead>*/}
		         <tbody>
		        <tr>
		         
		          <td className = "tdName">{firstName}</td>
		          <td className = "tdDescription">{lastName}</td>
		          <td className = "tdPrice">{address}</td>
		           <td className = "tdIsactive">{email}</td>
		           <td className = "tdIsactive">{isAdmin}</td>
		        </tr>
		        </tbody>
		     
		      </Table>
			
		</Container>
		)
	}

	// checks the validity of the PropTypes
	User.propTypes = {
		// "shape" mtehod is to check if the prop object conforms to a sepecific shape
		user: PropTypes.shape({
			firstName: PropTypes.string.isRequired,
			lastName: PropTypes.string.isRequired,
			address: PropTypes.string.isRequired,
			email: PropTypes.string.isRequired,
			isAdmin: PropTypes.string.isRequired
		})
	}