import { useState, useEffect, useContext } from 'react';
import React from 'react';
import { Fragment } from 'react'
import {Link, NavLink} from 'react-router-dom';
import { Row, Col, Button, Form} from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useHistory } from 'react-router-dom'
import { Container} from 'react-bootstrap';
import UserContext from '../UserContext';
import AdminButton from '../components/AdminButton'

export default function AddProduct(){

		// add codes for activity------------------------
		  const {user, setUser} = useContext(UserContext)
		 // end ---------------------


		// state hooks to store the value to input fields
		const [name,setName] = useState('');
		const [description, setDescription] = useState('');
		const [price, setPrice] = useState('');
		const [image, setImage] = useState('');
		let history = useHistory();
		
		
		//  function to simulate user registration
			async function registerProduct(e){
				 // Prevents page redirection via form submission
		        e.preventDefault();
		        
		        fetch('http://localhost:4000/products/product', {
	                		method: 'POST',
	                		headers:{
	                		    'Content-Type':'application/json'
	                		},
	                		body:JSON.stringify({
	                				name:name,
	                				description:description,
	                				image:image,
	                			    price:price

	                		})

	                	})
	                	.then(res => res.json())
	                	.then(data => {
	                		console.log(`${data} dataDATA`);
	                		Swal.fire({
	                			title:"Add Product Successfully",
	                			icon:"success",
	                			text:""
	                		});
	                		{/*<Redirect to ="/login"/>*/}
	                		history.push('/admin')
	                	})

	                }


return(

	<Fragment>
		
		<Container>
			 <h3 className= "mt-5">Welcome Admin</h3>
		</Container>
		<Container fluid className= "pl-5">
		  <Row>
		    <Col sm={3}>

		    <AdminButton />
		  	
		    </Col>
		    <Col sm={9} className="pl-5">
		   {/*<Link className="btn btn-primary controlButton" variant="primary" size="lg"  to="/AddProduct">Add</Link>*/}
		    <Button className="btn btn-primary controlButton" variant="primary"	 type="submit" as={NavLink} to="/addproduct">Add a product</Button>
		    <hr className="hr"></hr>
		    	<Container>
		    			<Form  onSubmit = {(e)=> registerProduct(e)} >
		    			  <Form.Group controlId="formBasicEmail">
		    			    <Form.Label>Product Name</Form.Label>
		    			    <Form.Control
		    			    type="text" 
		    			    placeholder="Product Name"
		    			    value={name}
		    			    onChange = { e => setName(e.target.value)}
		    			    required
		    			     />
		    			  </Form.Group>

		    			  <Form.Group controlId="formBasicPassword">
		    			    <Form.Label>Description</Form.Label>
		    			    <Form.Control
		    			    	type="text" 
		    			    	placeholder="Product Description"
		    			    	value={description}
		    			    	onChange = { e => setDescription(e.target.value)}
		    			    	required

		    			     />
		    			  </Form.Group>
		    			   <Form.Group controlId="formBasicPassword">
		    			    <Form.Label>Price</Form.Label>
		    			    <Form.Control
		    			    	type="text" 
		    			    	placeholder="Product Price"
		    			    	value={price}
		    			    	onChange = { e => setPrice(e.target.value)}
		    			    	required

		    			     />
		    			  </Form.Group>
		    			   <Form.Group controlId="formBasicPassword">
		    			    <Form.Label>Image</Form.Label>
		    			    <Form.Control
		    			    	type="text" 
		    			    	placeholder="Image URL"
		    			    	value={image}
		    			    	onChange = { e => setImage(e.target.value)}
		    			    	required

		    			     />
		    			  </Form.Group>

		    			<Button variant="primary" type="submit" id="submitBtn">
		    								   	  		     Add Product
		    								   	  		  </Button>
		    			</Form>
		    			</Container>
		    	
		    </Col>
		  </Row>
		</Container>
		</Fragment>
	)
}
	{/*<Container>
		<Form  onSubmit = {(e)=> registerProduct(e)} >
		  <Form.Group controlId="formBasicEmail">
		    <Form.Label>Product Name</Form.Label>
		    <Form.Control
		    type="text" 
		    placeholder="Product Name"
		    value={name}
		    onChange = { e => setName(e.target.value)}
		    required
		     />
		  </Form.Group>

		  <Form.Group controlId="formBasicPassword">
		    <Form.Label>Description</Form.Label>
		    <Form.Control
		    	type="text" 
		    	placeholder="Product Description"
		    	value={description}
		    	onChange = { e => setDescription(e.target.value)}
		    	required

		     />
		  </Form.Group>
		   <Form.Group controlId="formBasicPassword">
		    <Form.Label>Price</Form.Label>
		    <Form.Control
		    	type="text" 
		    	placeholder="Product Price"
		    	value={price}
		    	onChange = { e => setPrice(e.target.value)}
		    	required

		     />
		  </Form.Group>

		<Button variant="primary" type="submit" id="submitBtn">
							   	  		      Register now
							   	  		  </Button>
		</Form>
		</Container>
	)
}*/}