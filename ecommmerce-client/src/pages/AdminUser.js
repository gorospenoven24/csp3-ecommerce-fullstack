import React from 'react';
import {useEffect, useState } from 'react';
import { Fragment } from 'react'
import { Container} from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom';
import { Row, Col, Button, Form, tr, td, th, thead,Table, tbody} from 'react-bootstrap';
import Card from 'react-bootstrap/Card'
import Img from 'react-bootstrap/Image'
import ButtonGroup from 'react-bootstrap/ButtonGroup'
import UserContext from '../UserContext';
import {useContext } from 'react'
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import TableUser from '../components/TableUser'
import AdminButton from '../components/AdminButton'
// import AddProduct from 'pages/AddProduct'
// import Shop from '../Image/Shop.jpg';


export default function Admin(){
	const { user }=useContext(UserContext);
	const [user1, setUser1] = useState([]);
	useEffect(()=>{
		fetch('http://localhost:4000/users/alluser')
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			// set the courses state to map the data retrieved from the fetch request in several "Courescard"
			setUser1(data.map(user => {
					return (
						// key is to get the id of coursesData
						< TableUser key={user1.id} userProp={user}/>
					)
				})
			);
			
		})
	}, [])
	

	return (
		<Fragment>
		
		<Container>
			 <h3 className= "mt-5">Welcome Admin</h3>
		</Container>
		<Container fluid className= "pl-5">
		  <Row>
		    <Col sm={3}>

		    <AdminButton />
		  	
		    </Col>
		    <Col sm={9} className="pl-5">
		   {/*<Link className="btn btn-primary controlButton" variant="primary" size="lg"  to="/AddProduct">Add</Link>*/}
		    <Button className="btn btn-primary controlButton" variant="primary"	 type="submit" as={NavLink} to="/addproduct">Add a product</Button>
		    <hr className="hr"></hr>
		    	 <Table striped bordered hover size="sm">
		          <thead>
		            <tr>
		            
		              <th>Name</th>
		              <th>Description</th>
		              <th>Price</th>
		               <th>Status</th>
		            </tr>
		             </thead>
		             <tbody>
		            </tbody>
		            </Table>
		         {user1}
		          
		    	
		    </Col>
		  </Row>
		</Container>
		</Fragment>
	)
}