import React from 'react';
import { Fragment } from 'react'
import { Container} from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom';
import { Row, Col, Button, Form} from 'react-bootstrap';
import Card from 'react-bootstrap/Card'
import Img from 'react-bootstrap/Image'
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { useState, useEffect, useContext } from 'react';
import { useHistory } from 'react-router-dom'
import { Redirect } from 'react-router-dom';
import TableData from '../components/CartTable'
import Header from '../components/CartHeader'
import Footer from '../components/CartFooter'
import CartBody from '../components/CartBody'


export default function CartView(){

	return (
	             

			<Container className="mt-5">
				<Card>
				<Header />
				  <CartBody />
				  <Footer />
				</Card>

			</Container>
		
		)
}
