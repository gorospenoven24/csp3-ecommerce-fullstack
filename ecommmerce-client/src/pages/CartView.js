import React, { Fragment } from 'react';
import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, CardGroup } from 'react-bootstrap';

import AppNavbar from '../components/AppNavbar';
// import CartCheckOut from './CartCheckOut';
import ProductCard from '../components/ProductCard';



export default function CartView() {
	
	const [cart, setCart] = useState([]);
	const [cartItems, setCartItems] = useState([]);

	const userId = localStorage.getItem('id');

	useEffect(() => {
		fetch(`http://localhost:4000/carts/${userId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);			
			
			setCartItems(data[0].items.map(cartItem => {
				
				return(
					<ProductCard key={cartItem._id} cartProp={cartItem} />
					)
				}
			))
			console.log(cartItems);
			
		})
	}, [])

	const checkOut = async () => {

	}

	return(
	<Fragment>
		<AppNavbar/>
		<Row className="spacer"></Row>

		<Card>{cartItems}</Card>
	</Fragment>
	)
}
