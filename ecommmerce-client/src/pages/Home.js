import { Fragment, useEffect, useState } from 'react';
import ProductCard from '../components/ProductCard';
import { Container} from 'react-bootstrap';
import SecondNavbar from '../components/SecondNavbar';
import Banner from '../components/Banner'
// import coursesData from '../data/coursesData';


/*const [productName, setProductName] = useState('');
const [productPrice, setProductPrice] = useState('');
const [productQuantity, setProductQuantity] = useState('');
*/

export default function ProductList(){
	// console.log(coursesData)
	// console.log(coursesData[0])
	//  to get all the courses in coursecard

// State that will be use to store the course retrieve from the database
// useEffect is a use to fetch data

const [product, setProduct] = useState([]);

// const [product, setProduct] = useState([]);
	useEffect(()=>{
		fetch('http://localhost:4000/products/products')
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			// set the courses state to map the data retrieved from the fetch request in several "Courescard"
			setProduct(data.map(product => {
				console.log(product)
					return (
						// key is to get the id of coursesData
						<ProductCard key={product.id}  productProp={product}/>
					)
				})
			);
			
		})
	}, [product])

	const data={
		title: "Welcome To shopNow Enjoy Shopping",
	
	}


	return (
			<Fragment>
				{/*<SecondNavbar />*/}
					<Container fluid>
					 <Banner  data={data} />
				 	{product}
				</Container>
			</Fragment>
		
	)
}