import React from 'react';
import { Fragment } from 'react'
import { Container} from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom';
import { Row, Col, Button, Form} from 'react-bootstrap';
import Card from 'react-bootstrap/Card'
import Img from 'react-bootstrap/Image'
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { useState, useEffect, useContext } from 'react';
import { useHistory } from 'react-router-dom'
import { Redirect } from 'react-router-dom';

// import Shop from '../Image/Shop.jpg';


export default function Login(props	){
	        // allows us to comsume the User context object and its properties to use for user validation
	        const {user, setUser} = useContext(UserContext)

			// State hooks to store the values of the input fields
			const [email, setEmail] = useState('');
		    const [password, setPassword] = useState('');
		    // State to determine whether submit button is enabled or not
		    const [isActive, setIsActive] = useState(false);

		    let history = useHistory();


		    	    function authenticate(e) {

		    	        // Prevents page redirection via form submission
		    	        e.preventDefault();

		                // fetch request to process the backend API
		                // Syntax: fetch('url',{options})
		                //  .then(res=> res.json()).then(data => {})

		                fetch('http://localhost:4000/users/login', {
		                    method: 'POST',
		                    headers:{
		                        'Content-Type':'application/json'
		                    },
		                    body: JSON.stringify({
		                        email:email,
		                        password: password
		                    })
		                })
		                .then(res => res.json())
		                .then(data => {
		                    console.log(`from fetch ${data}`);

		                    // if no user information found, the "access" ptroperty will not be valid available and will return undefined
		                    if(typeof data.access !== "undefined"){
		                        // The token will be used to retrieve user information accross the whole frontend application and storing it in the local storage to alw ease of access to the user's information.
		                        localStorage.setItem('token', data.access);
		                        retrieveUserDetails(data.access);
		                        console.log(user)

		                        Swal.fire({
		                            title:"Login successfull",
		                            icon: "success",
		                            text:" Welcome to ShopNow!"
		                        })

		                    }
		                    else{
		                        Swal.fire({
		                            title:"Authentication Failed!",
		                            icon:"error",
		                            text: "Check your login details and try again."
		                        })
		                    }

		                })

		             
		    	        setEmail('');
		    	        setPassword('');
		    	        

		    	        // alert(`${email} has been verified! Welcome back!`);

		    	    }


		    	    // end of fetch===================================

		                const retrieveUserDetails = (token) =>{
		                    fetch('http://localhost:4000/users/details',{
		                        headers:{
		                            Authorization:`Bearer ${ token }`
		                        }
		                    })
		                       .then(res => res.json())
		                       .then(data => { 
		                        	console.log(`fetch from retrieveUserDetails ${data}`)   	    	
		                        		  setUser({
		                        		      id: data._id,
		                        		      isAdmin: data.isAdmin
		                        		})
		                     })
							
			            } 



		    		useEffect(() => {

		    	        // Validation to enable submit button when all fields are populated and both passwords match
		    	        if(email !== '' && password !== ''){
		    	            setIsActive(true);
		    	        }else{
		    	            setIsActive(false);
		    	        }

		    	    }, [email, password]);

		    	   	  localStorage.setItem('id', user.id);
		    	       localStorage.setItem('isAdmin', user.isAdmin);




	return (
	           (user.id !== null) ?
	            	(user.isAdmin === true) ?
	               		< Redirect to="/admin" />
	               		:
	               		< Redirect to="/" />
	             :
	             

			<Container className="mt-5">
					<Row>
					   <Col>
					   	
					   <img src="https://pbs.twimg.com/profile_images/895700558895566848/2oqW_o2b_400x400.jpg" className="imageLogo" />
					   </Col>
					   <Col>
					   	<Form onSubmit={(e) => authenticate(e)}>
					   	    <Form.Group className="mt-5" as={Col} controlId="formGridEmail">
					   	      <Form.Label className="mt-2">Email Address</Form.Label>
					   	      <Form.Control
					   	      	type="email" 
					   	      	placeholder="Enter email" 
					   	      	value={email}
					   	      	onChange={(e) => setEmail(e.target.value)}
					   	      	required
					   	       />
					   	    </Form.Group>

					   	    <Form.Group className="mt-3" as={Col} controlId="formGridPassword">
					   	      <Form.Label className="mt-2">Password</Form.Label>
					   	      <Form.Control 
					   	      type="password" 
					   	      placeholder="Password" 
					   	      value={password}
					   	      onChange={(e) => setPassword(e.target.value)}
					   	      required
					   	       />
					   	    </Form.Group>
					   	    
					   	  	 	  <Button className="mt-2 ml-3" style={{width:500}}variant="primary" type="submit">
					   	  	    		<h4>Login</h4> 
					   	  	   	</Button>
					   	  	   
					   	  
					   	  	 <Form.Group className="mt-4" as={Col} controlId="formGridPassword">
					   	     		< a href="/"  className="mt-4 ml-3" >Forgot Password</a>
					   	     		< a href="/Register"  className="ml-5" >Not yet member register here</a>
					   	    </Form.Group>
					   	</Form>
					   </Col>
					 </Row>
			</Container>
		
		)
}
