import { useContext, useEffect } from 'react';
// to redirect go back to our log in
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout(){
	const {unsetUser, setUser}	=useContext(UserContext);

	unsetUser();

	useEffect(() =>{
	// set the user sate back to its original value
		setUser({id:null});
	})

	// Redirect bak to login
	return (
		<Redirect to = "/login" />
		)
}