import { useState, useEffect, useContext } from 'react';
import React from 'react';
import { Fragment } from 'react'
import { Container} from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom';
import { Row, Col, Button, Form} from 'react-bootstrap';
import Card from 'react-bootstrap/Card'
import Img from 'react-bootstrap/Image'
import UserContext from '../UserContext';
import { Redirect } from 'react-router-dom';
import Swal from 'sweetalert2';
import { useHistory } from 'react-router-dom'
// history.push('/login')
// import { useHistory } from 'react-router-dom';


// import Shop from '../Image/Shop.jpg';


export default function Register(){
		// add codes for activity------------------------
		  const {user, setUser} = useContext(UserContext)
		 // end ---------------------


		// state hooks to store the value to input fields
		const [email,setEmail] = useState('');
		const [firstName, setFirstName] = useState('');
		const [lastName, setLastName] = useState('');
		const [address, setAddress] = useState('');
		const [mobileNo,setMobileNo] = useState('');
		// const [isAdmin,setisAdmin] = useState('');
		const [password1, setPassword1] = useState('');
		const [password2, setPassword2] = useState('');

		// state to determine wether the submit button is enabled or not
		const [isActive, setIsActive] = useState(false);
		let history = useHistory();
		console.log(email);
		console.log(password1);
		console.log(password2);

		//  function to simulate user registration
			function registerUser(e){
				 // Prevents page redirection via form submission
		        e.preventDefault();

	       fetch('http://localhost:4000/users/checkEmail', {
	                method: 'POST',
	                headers:{
	                    'Content-Type':'application/json'
	                },
	                body: JSON.stringify({
	                     email:email
	                  
	                })
	            })
	            .then(res => res.json())
	            .then(data => {
	                if(data === false){
	                	fetch('http://localhost:4000/users/register', {
	                		method: 'POST',
	                		headers:{
	                		    'Content-Type':'application/json'
	                		},
	                		body: JSON.stringify({
	                			firstName:firstName,
	                			lastName:lastName,
	                			 address:address,
	                		     email:email,
	                		     mobileNo:mobileNo,
	                		     password: password1
	                		   
	                		  
	                		})

	                	})
	                	.then(res => res.json())
	                	.then(data => {
	                		console.log(`${data} dataDATA`);
	                		Swal.fire({
	                			title:"registration Successfully",
	                			icon:"success",
	                			text:"welcome to zuitt"
	                		});
	                		{/*<Redirect to ="/login"/>*/}
	                		history.push('/login')
	                	})

	                }
	                else{
	                	Swal.fire({
	                			title:"Email is already exist",
	                			icon:"error",
	                			text: "Check your Email details and try again."
	                		});
	               		 }

	            })


			
				setFirstName('');
				setLastName('');
				setAddress('');
				setEmail('');
				setMobileNo('');
				setPassword1('');
				setPassword2('');

				// alert('Thankyou for registering')
			}

		useEffect(() =>{
			// validate to enable submit button when all fiels are populated and both passwords are match

			if ((firstName !== ''  && lastName !== ''  && address !== ''  && email !== ''  &&  mobileNo !== ''  &&  password1 !== '' && password2 !== '') && ( password1 === password2  ))
			{
				setIsActive(true);
			}
			else{
				setIsActive(false);
			}
		},[firstName,lastName,address, mobileNo,email, password1,password2])

		

		return (
			// add codes for activity----------------------------
				 (user.id !== null) ?
	               history.push('/login')
	             :
	             // end ---------------------


	

			<Container className="mt-5">
					<Row>
					   <Col>
					   	
					   	<img src="https://pbs.twimg.com/profile_images/895700558895566848/2oqW_o2b_400x400.jpg" className="imageLogo" />
					   </Col>
					   <Col>
					   	
					   	<Form onSubmit = {(e)=> registerUser(e)} >
					   	  <Row className="mb-3">
					   	    <Form.Group as={Col} controlId="formGridEmail">
					   	      <Form.Label>First Name</Form.Label>
					   	      <Form.Control
					   	       type="text" 
					   	       placeholder="First name"
					   	       value={firstName}
					   	       onChange = { e => setFirstName(e.target.value)}
					   	       required  
					   	       />
					   	    </Form.Group>

					   	    <Form.Group as={Col} controlId="formGridPassword">
					   	      <Form.Label>Last Name</Form.Label>
					   	      <Form.Control 
					   	      type="text" 
					   	      placeholder="Last Name" 
					   	       value={lastName}
					   	       onChange = { e => setLastName(e.target.value)}
					   	       required/>
					   	    </Form.Group>
					   	  </Row>

					   	  <Form.Group className="mb-3" controlId="formGridAddress1">
					   	    <Form.Label>Address</Form.Label>
					   	    <Form.Control 
					   	    type="text"
					   	    placeholder="1234 Main St"
					   	    value={address}
					   	    onChange = { e => setAddress(e.target.value)}
					   	    required />
					   	  </Form.Group>

					   	  <Form.Group className="mb-3" controlId="formGridAddress2">
					   	    <Form.Label>Celphone Number</Form.Label>
					   	    <Form.Control 
					   	    	type="text"
					   	    		placeholder="" 
					   	    	value={mobileNo}
					   	    	onChange = { e => setMobileNo(e.target.value)}
					   	    	required
					   	   />
					   	  </Form.Group>
					   	  	<Row className="mb-3">
					   	  	  <Form.Group as={Col} controlId="formGridEmail">
					   	  	    <Form.Label>Email</Form.Label>
					   	  	    <Form.Control
									type="email" 
							    	placeholder="Enter email" 
							    	value={email}
							    	onChange = { e => setEmail(e.target.value)}
							    	required 
					   	  	      />
					   	  	  </Form.Group>

					   	  	  <Form.Group as={Col} controlId="formGridPassword">
					   	  	    <Form.Label>Password</Form.Label>
					   	  	    <Form.Control type="password" 
					   	  	    	type="password" 
					   	  	    	placeholder="Password" 
					   	  	    	value = {password1}
					   	  	    	onChange = {e => setPassword1(e.target.value)}
					   	  	    	required
					   	  	     />
					   	  	  </Form.Group>
					   	  	</Row>
					   	  	   <Form.Group className="mb-3" controlId="formGridAddress2">
					   	    <Form.Label>Verify Password</Form.Label>
					   	    <Form.Control 
					   	    	type="password" 
					   	    	placeholder="Verify Password" 
					   	    	value = {password2}
					   	    	onChange = {e => setPassword2(e.target.value)}
					   	    	required 
					   	    	 />
					   	    
					   	  </Form.Group>
					   	  
					   	  	{ isActive ?
					   	  		  <Button variant="primary" type="submit" id="submitBtn">
					   	  		      Register now
					   	  		  </Button>
					   	   	 :
					   	     		 <Button variant="danger" type="submit" id="submitBtn" disabled>
					   	     		  Register now
					   	   	 	</Button>

					   	  	 }

					   	</Form>
					   </Col>
					 </Row>
			</Container>
		)
	
}

