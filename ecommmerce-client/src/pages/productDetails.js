import React from 'react';
import { useState, useEffect } from 'react';
import { Container, Card, Button, Row, Col, Span, Form } from 'react-bootstrap';
import CartView from './CartView';

import ProductCard from '../components/ProductCard';



export default function ProductView(props) {
	console.log(props);
	console.log(props.match.params.productId);
	const userId = localStorage.getItem('id');
	const productId = props.match.params.productId;

	const [name, setName] = useState([]);
	const [description, setDescription] = useState([]);
	const [price, setPrice] = useState([]);

	useEffect(() => {
		fetch(`http://localhost:4000/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
				setName(data.name);
				setDescription(data.description);
				setPrice(data.price);
				}
			);
	}, [])
	

	const addToCart = async () => {
        fetch(`http://localhost:4000/carts/${userId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                customerId: userId,
				productId: productId,
				price: price
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(`fetch for carts${data}`);
   	    })
	}

	return(
		<Container className="mt-5" style={{width:700}}>
				<Card>
					<Card.Body >
						  <Card.Header as="h5" className="header"> 
						  		
      								<Form.Label className="productDetails">
      									{name}
      								</Form.Label>
      						</Card.Header>
      						<Card.Subtitle as="h5" className="mt-2 ml-3"> 
						  			<Form.Label className="mr-3">
      									Description:
      								</Form.Label>
      								<Form.Label className="productDetails">
      									{description}
      								</Form.Label>
      						</Card.Subtitle>
      						<Card.Subtitle as="h5" className="mt-2 ml-3"> 
						  			<Form.Label className="mr-3">
      									Price:
      								</Form.Label>
      								<Form.Label className="productDetails">
      									{price}
      								</Form.Label>
      						</Card.Subtitle>
							<hr></hr>
							
						<Button variant="primary" size="lg" style={{width:620}} onClick={addToCart} to={`/carts/${userId}`} block>Add to Cart</Button>
					</Card.Body>
				</Card>
		</Container>
	)
}
